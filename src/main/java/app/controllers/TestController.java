package app.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import app.models.Customer;
import app.models.Loan;
import app.models.LogType;
import app.models.ServiceLog;
import app.persistence.SalesForceDAO;
import app.utilities.Util;

@RequestMapping("/test")
@Controller
public class TestController {
	private static final Logger logger = LoggerFactory.getLogger(RestController.class);
	@Autowired
	private SalesForceDAO sales;

	
	@RequestMapping(value = "/getCustomerByCellphone", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Customer getCustomerByCellphone(HttpServletRequest request) {
		String cellphone = null;
		cellphone = request.getParameter("cellphone");
		cellphone = cellphone.split("@")[0];
		logger.debug("cellphone: "+ cellphone);
		System.err.println(cellphone);
		if(cellphone != null && !cellphone.equals("") ){
			Customer Customer = sales.findCustomerByCellphone(cellphone);
			return Customer;
		}
		return new Customer();
		
	}
	
	@RequestMapping(value = "/getLoanByCellphone", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Loan getLoanByCellphone(HttpServletRequest request) {
		String cellphone = null;
		cellphone = request.getParameter("cellphone");
		cellphone = cellphone.split("@")[0];
		logger.debug("cellphone: "+ cellphone);
		System.err.println(cellphone);
		if(cellphone != null && !cellphone.equals("") ){
			Loan loan = sales.findLoanByCellphone(cellphone);
			return loan;
		}
		return new Loan();
		
	}
	
	@RequestMapping("/logService")
	@ResponseBody
	public ServiceLog setServiceLog(HttpServletRequest request, @RequestParam String customerId, @RequestParam String log){
		//ServiceLog slog = new ServiceLog("a03o000000Kds7UAAR");
		ServiceLog slog = new ServiceLog(customerId);
		if(LogType.Login.getValue().contains(log)){
			slog.setSearch_Type__c(LogType.Login);
		}
		else if(LogType.Balance.getValue().contains(log)){
			slog.setSearch_Type__c(LogType.Balance);
		}
		else if(LogType.NextPaymentAmount.getValue().contains(log)){
			slog.setSearch_Type__c(LogType.NextPaymentAmount);
		}
		else if(LogType.NextPaymentDate.getValue().contains(log)){
			slog.setSearch_Type__c(LogType.NextPaymentDate);
		}
		else{
			return slog;
		}
//		System.err.println("SLOG"+slog.getSearch_Type__c().getValue());
//		Customer customer=new Customer();
//		customer.setCellphone__c("573147920407");
//		customer.setEmail__c("molaya@vertical.com.co");
//		customer.setFirst_Name__c("Marlon");
//		customer.setId_Customer__c("1020433200");
//		customer.setId("a03o000000Kds7UAAR");
//		customer.setName("Marlon Olaya");
//		slog.setCustomer__c(customer);
		return sales.insertServiceLog(slog);
	}
	
	@RequestMapping("/hello")
	@ResponseBody
	public String hi(){
		return "Hello World!!, We are running!";
	}

}
