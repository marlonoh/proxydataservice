package app.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import app.models.Customer;
import app.models.Loan;
import app.models.LogType;
import app.models.ServiceLog;
import app.persistence.SalesForceDAO;
import app.persistence.SalesForceDBC;
import app.utilities.Util;

@Controller
public class RestController {
	
	private static final Logger logger = LoggerFactory.getLogger(RestController.class);
	@Autowired
	private SalesForceDAO sales;
	//private SalesForceDBC sales = new SalesForceDBC();
	private List<Loan> loans = new ArrayList<Loan>();
	
	/**
	 * Get Loan Information By Cellphone
	 * @param request [cellphone] Cellphone of user registered
	 * @return
	 */
	@RequestMapping(value = "/getLoanByCellphone", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Loan getLoanByCellphone(HttpServletRequest request, @RequestBody String json) {
		Map map = Util.jsonToMap(json);
		String cellphone = null;
		if(map != null && map.get("cellphone") != null && !map.get("cellphone").equals("")){
			cellphone = String.valueOf(map.get("cellphone"));	
		} else 
		if(cellphone == null || cellphone.equals("")){
			cellphone = request.getParameter("cellphone");
			
		}
		cellphone = cellphone.split("@")[0];
		logger.debug("cellphone: "+ cellphone);
		System.err.println(cellphone);
		if(cellphone != null && !cellphone.equals("") ){
			Loan loan = sales.findLoanByCellphone(cellphone);
			return loan;
		}
		return new Loan();
		
	}
	
	/**
	 * Get Loan Information By Cellphone
	 * @param request [cellphone] Cellphone of user registered
	 * @return
	 */
	@RequestMapping(value = "/getLoansByCellphone", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public List<Loan> getLoansByCellphone(HttpServletRequest request, @RequestBody String json) {
		Map map = Util.jsonToMap(json);
		List<Loan> resultList = new ArrayList<Loan>();
		String cellphone = null;
		if(map != null && map.get("cellphone") != null && !map.get("cellphone").equals("")){
			cellphone = String.valueOf(map.get("cellphone"));	
		} else 
		if(cellphone == null || cellphone.equals("")){
			cellphone = request.getParameter("cellphone");
			
		}
		cellphone = cellphone.split("@")[0];
		logger.debug("cellphone: "+ cellphone);
		System.err.println(cellphone);
		if(cellphone != null && !cellphone.equals("") ){
			resultList = sales.findLoansByCellphone(cellphone);
			
		}
		return resultList;
		
	}

	
	/**
	 * Get Loan Information By Cellphone
	 * @param request [cellphone] Cellphone of user registered
	 * @return
	 */
	@RequestMapping(value = "/getCustomerByCellphone", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Customer getCustomerByCellphone(HttpServletRequest request, @RequestBody String json) {
		Map map = Util.jsonToMap(json);
		String cellphone = null;
		if(map != null && map.get("cellphone") != null && !map.get("cellphone").equals("")){
			cellphone = String.valueOf(map.get("cellphone"));	
		} else 
		if(cellphone == null || cellphone.equals("")){
			cellphone = request.getParameter("cellphone");
			
		}
		cellphone = cellphone.split("@")[0];
		logger.debug("cellphone: "+ cellphone);
		System.err.println(cellphone);
		if(cellphone != null && !cellphone.equals("") ){
			Customer Customer = sales.findCustomerByCellphone(cellphone);
			return Customer;
		}
		return new Customer();
		
	}
	
	@RequestMapping(value = "/loggingService", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ServiceLog setLogService(HttpServletRequest request, @RequestBody String json) {
		Map map = Util.jsonToMap(json);
		ServiceLog slog = new ServiceLog();
		String aux = null;
		String log = null;
		if(map != null && map.get("logname") != null && !map.get("logname").equals("")
				&& map.get("logtype") != null && !map.get("logtype").equals("")
				&& map.get("customerid") != null && !map.get("customerid").equals("")){
			slog.setCustomer__c(String.valueOf(map.get("customerid")));
			slog.setName(String.valueOf(map.get("logname")));
			log = String.valueOf(map.get("logtype"));
			if(LogType.Login.getValue().contains(log)){
				slog.setSearch_Type__c(LogType.Login);
			}
			else if(LogType.Balance.getValue().contains(log)){
				slog.setSearch_Type__c(LogType.Balance);
			}
			else if(LogType.NextPaymentAmount.getValue().contains(log)){
				slog.setSearch_Type__c(LogType.NextPaymentAmount);
			}
			else if(LogType.NextPaymentDate.getValue().contains(log)){
				slog.setSearch_Type__c(LogType.NextPaymentDate);
			}
			else{
				return slog;
			}
			
		} else
			return slog;
		System.err.println("slog: "+ slog.toString());
		return sales.insertServiceLog(slog);
	}


}
