package app.models;

import lombok.Data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Customer {
	@JsonProperty(value="Id")
	private String Id;
	@JsonProperty(value="Name")
	private String Name;
	private String cellphone__c;
	private String first_Name__c;
	private String email__c;
	private String id_Customer__c;
	
}
