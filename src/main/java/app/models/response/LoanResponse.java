package app.models.response;

import java.util.List;

import lombok.Data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import app.models.Loan;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanResponse {
	
	private Integer totalSize;
	private String done;
	private List<Loan> records;
	
	public LoanResponse() {
		super();
	}
	
	public Integer getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

	public List<Loan> getRecords() {
		return records;
	}
	public String getDone() {
		return done;
	}
	public void setDone(String done) {
		this.done = done;
	}
	public void setRecords(List<Loan> records) {
		this.records = records;
	}
	
	
}
