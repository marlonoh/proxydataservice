package app.models.response;

import java.util.List;

import app.models.Customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Customer JSON Response by SalesForce Query.
 * @author molaya
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class CustomerResponse {
	private Integer totalSize;
	private String done;
	private List<Customer> records;
	public CustomerResponse() {
		super();
	}
	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize() {
		return totalSize;
	}
	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}
	/**
	 * @return the done
	 */
	public String getDone() {
		return done;
	}
	/**
	 * @param done the done to set
	 */
	public void setDone(String done) {
		this.done = done;
	}
	/**
	 * @return the records
	 */
	public List<Customer> getRecords() {
		return records;
	}
	/**
	 * @param records the records to set
	 */
	public void setRecords(List<Customer> records) {
		this.records = records;
	}
	
}
