package app.models;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServiceLog {
	//@JsonProperty(value="Id")
	private String Id;
	private LogType search_Type__c;
	private String customer__c;
	private String Name;
	
	public ServiceLog() {
		super();
	}
	
	public ServiceLog(String customer){
		customer__c = customer; 
	}

	public String getId() {
		return Id;
	}
	public void setId(String id) {
		this.Id = id;
	}
	public LogType getSearch_Type__c() {
		return search_Type__c;
	}
	public void setSearch_Type__c(LogType search_Type__c) {
		this.search_Type__c = search_Type__c;
	}
	public String getCustomer__c() {
		return customer__c;
	}
	public void setCustomer__c(String customer__c) {
		this.customer__c = customer__c;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	
}
