package app.models;

import lombok.Data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Loan {
	@JsonProperty(value="Balance__c")
	private Integer Balance__c;
	@JsonProperty(value="Next_Payment_Amount__c")
	private Integer Next_Payment_Amount__c;
	@JsonProperty(value="Next_Payment_Date__c")
	private String Next_Payment_Date__c;
	@JsonProperty(value="Id")
	private String Id;
	@JsonProperty(value="Name")
	private String Name;
		
}
