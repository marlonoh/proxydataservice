package app.models;

/**
 * List of values for log info. 
 * @author molaya
 */
public enum LogType {
	Login("Login"),
	Balance("Balance"),
	NextPaymentAmount("Next Payment Amount"),
	NextPaymentDate("Next Payment Date");

	private String value;
	
	LogType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
