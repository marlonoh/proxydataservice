/**
 * 
 */
package app.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ch.qos.logback.classic.db.DBAppender;
import app.models.Customer;
import app.models.response.CustomerResponse;
import app.models.Loan;
import app.models.response.LoanResponse;
import app.models.ServiceLog;
import app.utilities.Util;

/**
 * @author molaya
 *
 */
@Component
public class SalesForceDAOImpl implements SalesForceDAO {

	private SalesForceDBC db;
	private LoanResponse loans;
	private static final Logger logger = LoggerFactory
			.getLogger(SalesForceDBC.class);

	@Autowired
	public SalesForceDAOImpl(SalesForceDBC db) {
		this.db = db;
		if (!this.db.isConnected()) {
			this.db.connect();
			System.out.println("First Connection to SalesForce");
			// StdOutHandle.add("Configuring users on database");
		}
		this.loans = new LoanResponse();
	}
	
	@Override
	public Loan findLoanById(String id) {
		String sfquery = "?q=SELECT+Name,Balance__c,Next_Payment_Amount__c,Next_Payment_Date__c+FROM+Loan__c+WHERE+Customer__r.cellphone__c+=+'"
				+ id + "'";
		String serviceResponse = db.execute("GET", sfquery, "application/json");
		logger.debug("Service Response \n" + serviceResponse);

		LoanResponse loanResponse = (LoanResponse) Util.jsonToObject(
				serviceResponse, new LoanResponse());

		if (loanResponse != null && loanResponse.getRecords() != null
				&& !loanResponse.getRecords().isEmpty()) {
			return loanResponse.getRecords().get(0);
		}
		return new Loan();
	}
	
	@Override
	public Loan findLoanByCellphone(String cellphone) {
		String sfquery = "?q=SELECT+Name,Id,Balance__c,Next_Payment_Amount__c,Next_Payment_Date__c+FROM+Loan__c+WHERE+Customer__r.cellphone__c+=+'"
				+ cellphone + "'";
		String serviceResponse = db.executeQuery(sfquery, "GET");
		System.err.println(serviceResponse);
		LoanResponse loanResponse = (LoanResponse) Util.jsonToObject(
				serviceResponse, new LoanResponse());
		if (loanResponse != null && loanResponse.getRecords() != null
				&& !loanResponse.getRecords().isEmpty()) {
			return loanResponse.getRecords().get(0);
		}
		return new Loan();
	}
	
	@Override
	public List<Loan> findLoansByCellphone(String cellphone) {
		String sfquery = "?q=SELECT+Name,Id,Balance__c,Next_Payment_Amount__c,Next_Payment_Date__c+FROM+Loan__c+WHERE+Customer__r.cellphone__c+=+'"
				+ cellphone + "'";
		String serviceResponse = db.executeQuery(sfquery, "GET");
		System.err.println(serviceResponse);
		LoanResponse loanResponse = (LoanResponse) Util.jsonToObject(
				serviceResponse, new LoanResponse());
		if (loanResponse != null && loanResponse.getRecords() != null
				&& !loanResponse.getRecords().isEmpty()) {
			return loanResponse.getRecords();
		}
		return new ArrayList<Loan>();
	}
	
	@Override
	public Customer findCustomerByCellphone(String cellphone) {
		String sfquery = "?q=SELECT+Name,Id,cellPhone__c,id_Customer__c,first_Name__c,email__c+FROM+Customer__c+WHERE+Cellphone__c+=+'"
				+ cellphone + "'";
		String serviceResponse = db.executeQuery(sfquery, "GET");
		System.err.println(serviceResponse);
		CustomerResponse customerResponse = (CustomerResponse) Util.jsonToObject(
				serviceResponse, new CustomerResponse());
		if (customerResponse != null && customerResponse.getRecords() != null
				&& !customerResponse.getRecords().isEmpty()) {
			return customerResponse.getRecords().get(0);
		}		
		return new Customer();
	}
	
	/**
	 * Insert log
	 * @param lead
	 * @return
	 */
	@Override
	public ServiceLog insertServiceLog(ServiceLog object){
		object = db.executeUpdate("/registro_Consultas__c", "POST", object);
		if (object != null && object.getId()!=null){
			System.err.println("ServiceLog Created");
			return object;
		}
		return null;
	}

}
