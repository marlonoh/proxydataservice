package app.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import app.models.Customer;
import app.models.Loan;
import app.models.ServiceLog;

@Component
public interface SalesForceDAO {

	public Loan findLoanById(String id);
	public Customer findCustomerByCellphone(String cellphone);
	public Loan findLoanByCellphone(String cellphone);
	public ServiceLog insertServiceLog(ServiceLog log);
	public List<Loan> findLoansByCellphone(String cellphone);
	
}
