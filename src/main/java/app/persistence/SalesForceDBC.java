package app.persistence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import app.models.SalesForceProperties;
import app.models.ServiceLog;
import app.utilities.Util;

@Component
public class SalesForceDBC {
	private String sessionId;
	private String consumerKey;
	private String consumerSecret;
	private String username;
	private String password;
	private String staticSessionId;

	private static final Logger logger = LoggerFactory
			.getLogger(SalesForceDBC.class);
	private final static String INSTANCE_URL = "https://na17.salesforce.com/";
	private final static String SALESFORCE_BASE_QUERY = INSTANCE_URL +"services/data/v24.0/query";
	private final static String SALESFORCE_BASE_OBJECT = INSTANCE_URL +"services/data/v24.0/sobjects";
	private final static String SALESFORCE_BASE_APEX_SERVICES = INSTANCE_URL +"services/apexrest";

	@Autowired
	public SalesForceDBC(SalesForceProperties sfp){
		this.consumerKey = sfp.getConsumerKey();
		this.consumerSecret = sfp.getConsumerSecret();
		this.username = sfp.getUsername();
		this.password = sfp.getPassword();
	}
	
	public SalesForceDBC() {
		this.setConfigurationsByProperties();
		this.sessionId = createSessionId();
		System.err.println("The ID is= " + this.sessionId);
		// this.sessionId =
		// "00Do0000000ZbBV!ARQAQJNwXT8.iuBCLRjgW0ZuJKcW911BBldbfBjQK2HkkUbtoz3vhdajwTuWtcIwGveeHp.uRkDwG.eoA4RLypdv_GnmlPAg";
	}
	
	public boolean isConnected(){
		if(sessionId != null && sessionId.isEmpty() == false && sessionId != "")
			return true;
		return false;
	}
	
	public void connect(){
		this.sessionId = createSessionId();
	}

	public SalesForceDBC(String sessionId, String consumerKey,
			String consumerSecret, String username, String password) {
		super();
		this.sessionId = sessionId;
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
		this.username = username;
		this.password = password;
	}

	/**
	 * Execute a query in salesforce
	 * @param query
	 * @param sessionId
	 * @return
	 */
	public String executeQuery(String query, String httpType){
		logger.debug("Start Method");
		String queryResponse = execute(
				SALESFORCE_BASE_QUERY,
				httpType,
				query.replace(" ", "+"),//Replace blanks with '+'
				"application/json", createSessionId());
		return queryResponse;
	}
	
	/**
	 * Execute a Update or Insert to SalesForce ServiceLog
	 * @param query
	 * @param httpType [GTE, POST, PATCH]
	 * @return
	 */
	public ServiceLog executeUpdate(String urlObject, String httpType, ServiceLog object){
		String id = "";
		if (httpType.equalsIgnoreCase("PATCH")){
			id = "/"+object.getId();
			object.setId(null);
		}
		if (!urlObject.contains("/")){
			urlObject = "/" + urlObject; 
		}
		logger.debug("Start Method");
		String result = execute(
				SALESFORCE_BASE_OBJECT+urlObject+id,
				httpType,
				 Util.objetToJson(object),//Replace blanks with '+'
				"application/json", createSessionId());
		Map map = Util.jsonToMap(result);
		if (httpType.equalsIgnoreCase("PATCH"))
			id = String.valueOf(map.get("id"));
		else
			id = id.replace("/", "");
		if(result != null || result.equals("")){
			object.setId(id);
			return object; 
		}else{
			logger.error("Error:" +result);
		}
		return null;
		
	}
	
	public void setConfigurations(String consumerKey, String consumerSecret,
			String username, String password) {
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
		this.username = username;
		this.password = password;
	}

	public void setConfigurationsByProperties() {
		this.consumerKey = "3MVG9xOCXq4ID1uEVLM7A32fkJlzrA1loxe31.hcO4IAZKDwNvVgm1nyP3GQSH_nv6iR0hPRRJTE3KdJL74KJ";
		this.consumerSecret = "4418630027747017380";
		this.username = "jous32@gmail.com";
		this.password = "Oregano321";
	}

	private String createSessionId() {
		String urlParameters = "grant_type=password&client_id=" + consumerKey
				+ "&client_secret=" + consumerSecret + "&username=" + username
				+ "&password=" + password;

		String loginresponse = execute(
				"https://login.salesforce.com/services/oauth2/token", "POST",
				urlParameters, "application/x-www-form-urlencoded", null);
		String sessionId = getSessionId(loginresponse);
		return sessionId;
	}
	
	private String execute(String targetURL, String HttpMethod,
			String urlParameters, String contentType, String SessionId) {
		URL url;
		HttpURLConnection connection = null;
		try {
			url = new URL(targetURL);
			if ((HttpMethod == "GET" && SessionId == null) || (HttpMethod == "GET" &&  urlParameters.startsWith("?q=")) ) {
				url = new URL(url.toString() + "/" + urlParameters);
			} else	if( HttpMethod == "PATCH"){
				url = new URL(url.toString() + "?_HttpMethod=PATCH");
			}
			connection = (HttpURLConnection) url.openConnection();
			if (SessionId != null) {
				connection.setRequestProperty("Authorization", "Bearer "
						+ SessionId);
				connection.setRequestProperty("accept", "application/json");
			} else {
				connection.setRequestProperty("accept", "application/xml");
			}


			if (HttpMethod == "POST" || HttpMethod == "PATCH" ) {
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Length",
						"" + Integer.toString(urlParameters.getBytes().length));
				connection.setRequestProperty("Content-Type", contentType);
				connection.setRequestProperty("Content-Language", "en-US");
				connection.setUseCaches(false);
				connection.setDoInput(true);
				connection.setDoOutput(true);
				OutputStream os = connection.getOutputStream();
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(os, "UTF-8"));
				writer.write(urlParameters);
				writer.flush();
				writer.close();
				os.close();
			}else {
				connection.setRequestMethod(HttpMethod);
			}
			connection.connect();
//			connection.addRequestProperty("Authorization", "OAuth "
//					+ SessionId);
			InputStream is = null;
			if(connection.getResponseCode() == 200 || connection.getResponseCode() == 201 || connection.getResponseCode() == 204){ //200 Ok and 201 Created 204 Updated
				is = connection.getInputStream();
			}else{
				is = connection.getErrorStream();
			}
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		} catch (Exception e) {
			logger.error("Error:"+ e.getMessage(),e);
			return null;
		}

		finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}
	
	public String execute(String HttpMethod,
			String urlParameters, String contentType) {
		String SessionId = this.sessionId;
		
		String targetURL = SALESFORCE_BASE_QUERY;
		URL url;
		HttpURLConnection connection = null;
		try {
			url = new URL(targetURL);
			if (HttpMethod == "GET") {
				url = new URL(url.toString() + "/" + urlParameters);
			}
			connection = (HttpURLConnection) url.openConnection();
			if (SessionId != null) {
				connection.setRequestProperty("Authorization", "OAuth "
						+ SessionId);
				connection.setRequestProperty("accept", "application/json");
			} else {
				return null;
			}

			if (HttpMethod == "POST") {
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Length",
						"" + Integer.toString(urlParameters.getBytes().length));
				connection.setRequestProperty("Content-Type", contentType);
				connection.setRequestProperty("Content-Language", "en-US");
				connection.setUseCaches(false);
				connection.setDoInput(true);
				connection.setDoOutput(true);
				OutputStream os = connection.getOutputStream();
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(os, "UTF-8"));
				writer.write(urlParameters);
				writer.flush();
				writer.close();
				os.close();
			}

			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return (e.getClass().getName());
		}

		finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}


	static String getSessionId(String loingResponse) {
		java.io.InputStream sbis = new java.io.StringBufferInputStream(
				loingResponse.toString());
		javax.xml.parsers.DocumentBuilderFactory b = javax.xml.parsers.DocumentBuilderFactory
				.newInstance();
		b.setNamespaceAware(false);
		org.w3c.dom.Document doc = null;
		javax.xml.parsers.DocumentBuilder db = null;
		try {
			db = b.newDocumentBuilder();
			doc = db.parse(sbis);
		} catch (Exception e) {
			e.printStackTrace();
		}
		org.w3c.dom.Element element = doc.getDocumentElement();
		String access_token = "";
		NodeList nodeList = element.getElementsByTagName("access_token");
		if (nodeList != null && nodeList.getLength() > 0) {
			Element myElement = (Element) nodeList.item(0);
			access_token = myElement.getFirstChild().getNodeValue();
		}
		return access_token;
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}





