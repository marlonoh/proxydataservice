package app.utilities;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.PropertyNamingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.PascalCaseStrategy;


/**
 * common methods.
 * 
 * @author jmartinez
 * 
 */
public final class Util {

	/**
	 * Constructor.
	 */
	private Util() {
	}

	/**
	 * Path of AMPTUI properties file.
	 */
	private static final String INVOICEIVR_PROPERTIES = "/salesforce.properties";

	/**
	 * Key for date format.
	 */
	public static final String DATE_FORMAT = "DATE_FORMAT";

	/**
	 * {@link Logger}.
	 */
	private static Logger logger = LoggerFactory.getLogger(Util.class);

	/**
	 * Key for date format hour.
	 */
	public static String DATE_FORMAT_HOUR = "DATE_FORMAT_HOUR";

	/**
	 * Get a property from ampt-ui.properties.
	 * 
	 * @param key
	 *            key to search in the property file.
	 * @return Value of the key.
	 */
	public static String getPropertiesKey(String key) {

		// Load server base Url from properties file
		Properties prop = new Properties();
		try {
			prop.load(Util.class.getResourceAsStream(INVOICEIVR_PROPERTIES));
			return prop.getProperty(key);
		} catch (Exception e) {
			logger.error("Cant read the property " + key, e);
		}
		return null;

	}

	public static boolean testIpAddressWithPort(String ipAddress, int port) {
		InetSocketAddress endPoint = new InetSocketAddress(ipAddress, port);
		if (endPoint.isUnresolved()) {
			return false;
		} else {
			Socket socket = null;
			try {
				socket = new Socket();
				socket.connect(endPoint, 10000);
				return true;
			} catch (Exception ex) {
				logger.error("Could not connect to port: " + port + " of IP: "
						+ ipAddress + " exception: " + ex.toString());
				return false;
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (Exception ioe) {
						logger.error("Error closing socket on port: " + port
								+ " of IP: " + ipAddress + " " + ioe);
					}
				}
			}
		}
	}

	public static boolean pingAddress(String ipAddress) {
		try {
			logger.debug("Trying IP address: " + ipAddress + "... ");
			InetAddress inetHost = InetAddress.getByName(ipAddress);
			return inetHost.isReachable(10000);
		} catch (UnknownHostException ex) {
			logger.error("Unknown host trying IP address: " + ipAddress);
			return false;
		} catch (IOException e) {
			logger.error("IO exception trying IP address: " + ipAddress);
			return false;
		}
	}

	/**
	 * 
	 * @param date
	 *            Date to truncate
	 * @param startDay
	 *            true, truncate hour to 00:00:00, false truncate hour to
	 *            23:59:59
	 * @return Date with truncate hour.
	 */
	public static Calendar truncateHour(Calendar date, boolean startDay) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date.getTimeInMillis());
		if (startDay) {
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
		} else {
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
		}
		return calendar;

	}

	/**
	 * 
	 * @param date
	 *            Date to truncate
	 * @param startDay
	 *            true, truncate hour to 00:00:00, false truncate hour to
	 *            23:59:59
	 * @return Date with truncate hour.
	 */
	public static Calendar truncateHour(Date date, boolean startDay) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return truncateHour(calendar, startDay);
	}

	/**
	 * http://www.mkyong.com/java/how-to-convert-java-object-to-from-json-
	 * jackson/ Transform an object to JSON representation.
	 * 
	 * @param object
	 *            Object to transform.
	 * @return String in JSON format.
	 */
	public static String objetToJson(Object object) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * http://www.mkyong.com/java/how-to-convert-java-object-to-from-json-
	 * jackson/ Transform an JSON to object representation.
	 * 
	 * @param json
	 *            JSON representation of the object
	 * @param object
	 *            Destination object to transform.
	 * @return Object populated with the data of JSON.
	 */
	public static Object jsonToObject(String json, Object object) {
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			return mapper.readValue(json, object.getClass());
		} catch (JsonGenerationException e) {
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Return a map with the values string in XML
	 * 
	 * @param xml
	 *            the string in XML format
	 * @return {@link XPath}
	 * @throws ParserConfigurationException
	 * @throws IOException
	 *             {@link IOException}
	 * @throws SAXException
	 *             {@link SAXException}
	 * @throws UnsupportedEncodingException
	 *             {@link UnsupportedEncodingException}
	 * @throws XPathExpressionException
	 *             {@link XPathExpressionException}
	 */
	public static Map<String, String> getValues(String xml,
			String xpathExpresion) throws ParserConfigurationException,
			UnsupportedEncodingException, SAXException, IOException,
			XPathExpressionException {

		// http://viralpatel.net/blogs/java-xml-xpath-tutorial-parse-xml/
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory
				.newInstance();

		DocumentBuilder builder = builderFactory.newDocumentBuilder();

		// http://stackoverflow.com/questions/1706493/java-net-malformedurlexception-no-protocol
		Document xmlDocument = builder.parse(new InputSource(
				new ByteArrayInputStream(xml.getBytes("utf-8"))));

		XPath xPath = XPathFactory.newInstance().newXPath();

		NodeList nodeList = (NodeList) xPath.compile(xpathExpresion).evaluate(
				xmlDocument, XPathConstants.NODESET);

		Map<String, String> values = new HashMap<String, String>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			String nodeName = node.getNodeName();

			if (node.getChildNodes() != null
					&& node.getChildNodes().item(0) != null) {
				if (values.containsKey(nodeName)) {
					values.put(nodeName, values.get(nodeName) + " "
							+ node.getChildNodes().item(0).getNodeValue());
				} else {
					values.put(nodeName, node.getChildNodes().item(0)
							.getNodeValue());
				}

			} else {
				values.put(nodeName, "");
			}

		}
		return values;
	}

	/**
	 * Convert a string JSON to map.
	 * 
	 * @param content
	 *            String in JSON format
	 * @return Map with the values of JSON string.
	 */
	public static Map jsonToMap(String content) {

		if (content == null) {
			return null;
		}

		// http://stackoverflow.com/questions/3653996/how-to-parse-a-json-string-into-jsonnode-in-jackson
		// http://arkuarku.wordpress.com/2010/12/10/jackson-json-objectmapper/
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(content, Map.class);
		} catch (JsonMappingException e) {

			// try pasrse array
			try {
				mapper.readValue(content, Object.class);
				return null;
			} catch (JsonParseException e1) {
				throw new RuntimeException(e);
			} catch (JsonMappingException e1) {
				throw new RuntimeException(e);
			} catch (IOException e1) {
				throw new RuntimeException(e);
			}
		} catch (JsonParseException e) {

			// delete first and last { }
			content = content.substring(1, content.length() - 1);

			Map contentMap = new HashMap();
			String[] contentSplit = content.split(",");
			for (String value : contentSplit) {
				String[] keyAndValue = value.split("\"");

				if (keyAndValue.length == 4) {
					contentMap.put(keyAndValue[1], keyAndValue[3]);
				} else {
					StringBuilder complexValue = new StringBuilder();
					for (int i = 3; i < keyAndValue.length; i++) {
						complexValue.append(keyAndValue[i]);
					}
					if (keyAndValue.length > 1) {
						contentMap.put(keyAndValue[1], complexValue.toString());
					}
				}

			}

			return contentMap;

		} catch (EOFException e) {

			// the string isn't a well formed JSON
			return null;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * http://stackoverflow.com/questions/11424540/verify-if-string-is-
	 * hexadecimal
	 * 
	 * @param cadena
	 *            String to validate
	 * @return true if the number contain hex number, false otherwise
	 */
	public static boolean isHexNumber(String cadena) {
		try {
			Long.parseLong(cadena, 16);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	/**
	 * http://stackoverflow.com/questions/4303332/how-can-i-find-the-
	 * installation-directory-of-a-java-application
	 * 
	 * @return path where the application is installed
	 */
	public static String getInstallationPath() {
		Util util = new Util();
		return util.getClass().getProtectionDomain().getCodeSource()
				.getLocation().toString().replace("file:", "")
				.replace("WEB-INF/classes/", "");
	}

	/**
	 * Get the time zone to base based on UTC+0 UTC, time zone associated 0
	 * +0:00, 1 -1:00, 2 -2:00, 3 -3:00, 4 -4:00, 5 -5:00, 6 -6:00, 7 -7:00, 8
	 * -8:00, 9 -9:00, 10 -10:00, 11 -1:00 12 +12:00, 13 +11:00, 14 +10:00, 15
	 * +9:00, 16 +8:00, 17 +7:00, 18 +6:00, 19 +5:00, 20 +4:00, 21 +3:00, 22
	 * +2:00, 23 +1:00
	 * 
	 * @return [+/-]##:##
	 */
	public static String getTimeZone() {

		StringBuilder timeZone = new StringBuilder();

		Calendar now = Calendar.getInstance();

		int hour = now.get(Calendar.HOUR_OF_DAY);
		if (hour > 11) {
			timeZone.append("+").append(24 - hour).append(":");
		} else {
			timeZone.append("-").append(hour).append(":");
		}

		int minute = now.get(Calendar.MINUTE);
		if (minute >= 30) {
			timeZone.append("30");
		} else {
			timeZone.append("00");
		}

		return timeZone.toString();

	}

	/**
	 * http://stackoverflow.com/questions/4128436/query-string-manipulation-in-
	 * java Convert http query to map
	 * 
	 * @param query
	 *            http query
	 * @return map with key and values of http query
	 */
	public static Map<String, String> getQueryMap(String query) {
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String name = param.split("=")[0];
			String value = param.split("=")[1];
			map.put(name, value);
		}
		return map;
	}

}

